## Experimental Software Warning


**Mosaic** is experimental, **ALPHA** quality software.

**Catastrophic data loss** and **security vulnerabilities** should be **expected**.
==================================================================================================================

Mosaic is currently in the *Open Alpha Preview* release stage.

Alpha releases intended for evaluation purposes only.

**Recommended only for alpha testers and developers interested in distributing their applications for Mosaic.**

# Quick Start

Prerequisites
- Docker or Docker for Desktop (Linux, macOS or Windows)
- Minimum 8GB RAM
- Modern AMD64 or AARCH64 processor
- Familiar with command-line

Get started with a Fractal Networks hosted, custodial alpha deployment [console.fractalnetworks.co](https://console.fractalnetworks.co).

# Mosaic Introduction

Mosaic is an open source, noncustodial personal computing platform designed to make self-hosting easy and reliable. It combines an App Store with simple container orchestration, storage and connectivity solutions based on popular open source tools.

> Mosaic introduces several high-level technical concepts such as the Fractal Network, RPoVPN, portable self-hosted applications, collaborative-hosting, transitive digital trust, fork-able organizations among others. See the glossary for an overview of Mosaic's technical concepts.

Mosaic enables multi-device self-hosted deployment of popular free software applications, automatic updates, backups with point-in-time recovery and a robust distributed ingress connectivity solution making it possible to self-host reliably from home, with IPv6 or from behind a firewall or CGNAT (carrier-grade NAT)

Mosaic is free designed to be simple enough that you can take it apart and figure out how it works. We call it *manual-included software*. We built Mosaic to bootstrap a sustainable software ecosystem and escape vendor lock-in once and for all. We eschew technological centralization and walled gardens.

Mosaic is not yet perfect, it is still very much a work-in-progress with a
lot of opportunities for improving things. If you want to come on this journey with us,
we hope that you will have a good experience, and we will hope that you can help us improve
that experience for the others yet to come.

## Experimental Software Warning


**Mosaic** is experimental, **ALPHA** quality software.

**Catastrophic data loss** and **security vulnerabilities** should be **expected**.
==================================================================================================================

# Overview

Once logged into Mosaic, you will see something like this. This is the main page of Mosaic, which shows your devices (on the right), you applications (center) and your groups (on the left).

![Mosaic empty](../media/mosaic-empty.png)

# What is Self-Hosting?

![Software Freedom](https://images.unsplash.com/photo-1543357480-c60d40007a3f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2340&q=80)


> [Meet the Self-Hosters, Taking Back the Internet One Server at a Time](https://www.vice.com/en/article/pkb4ng/meet-the-self-hosters-taking-back-the-internet-one-server-at-a-time).

Self-hosting is about taking ownership of your digital existence; however, with that ownership comes the responsibility of security and maintenance. These responsibilities are simply too burdensome, not only for the typical consumer but also for a seasoned IT professional. Our mission at Fractal Networks is to provide individuals with the tools, education and support required to make self-hosting approachable, reliable and secure.

> Mosaic is a collection of next-generation free-and-open personal computing
> technologies designed to enable the user-owned, self-hosted Internet.

## Why Self-Hosting?

Before we can answer the why, allow us to explain a little bit about how the
internet works today. It's a short detour on our journey together, but you
should find it interesting if you have any interest in personal privacy or
personal freedom.

### Online Apps and Services

In the beginning, all apps were offline. Today, the oppostie is true.

A modern software application or service typically consists of two parts:


<figure markdown>
![Backend vs frontend, taken from https://www.seobility.net/en/wiki/Frontend, CC BY-SA 4.0](../media/frontend-backend.png){ width="300" }
  <figcaption>Backend vs Frontend, taken from <https://www.seobility.net/en/wiki/Frontend>, CC BY-SA 4.0</figcaption>
</figure>


- The **frontend** is the part of the application that you interact with, like
  an app on your phone or a website.
- The **backend** is the part that runs on a server in some datacenter
  somwhere. It stores your data and houses the bulk of the application's logic.
  You never directly interact with it. Remember, *the cloud is just someone
  else's computer, that happens to have access to all of your stuff*.

### Surveillance Capitalism

From the perspective of the consumer, the business model of the modern Internet
is surveillance capitalism which you can learn more about from Shoshanna
Zuboff's excellent book: *The Age of Surveillance Capitalism: The Fight for a
Human Future at the New Frontier of Power*.

> Early on, unscrupulous corporations realized they could provide online
> applications as "free" services in exchange for capitalizing on the user's
> personal data. Your data is used to improve their services (of course) but
> also to direct your attention towards anyone willing to pay the fair market
> price for it, or worse.


### Cloud Services

Most application's backends are powered by Cloud services that aren't even
owned or managed by the creator of the application. This may sound convenient
for consumers and app developers, but it comes at the cost of having to trust
the company who owns the infrastructure the backend runs on to properly secure your
data or risk it being stolen and most likely sold and that is only in the best
case. 

> The reality is that most online service providers are selling your personal
> information into a dark market of shadowy data brokers. Just search for
> yourself on truepeoplesearch.com -- just kidding -- don't do that, you'd be
> giving them another data point, like your current public IP which in turn
> would corroborate (or reveal) your current address.

### Custodial Computing

The computing reality we've described up to this point is best described as
*custodial* in nature. Users are entirely reliant on 3rd-party intermediaries
they know nothing about. Because of the custodial nature of modern computing
the individual has virtually no control or agency over their most important
personal data. Custodial computing is a far cry from the personal computing
future Steve Jobs envisioned with the advent of the Macintosh. Noncustodial
computing is an obvious solution for returning balance to the force but doing
so will require an awareness of the imbalance. 

### Self-Hosting

*Self-hosting* means that an application's backend is *hosted and owned by you*,
usually in your possession, at home, with minimal 3rd-parties involved. Most
importantly *a 3rd-party cannot revoke access to your self-hosted applications
or the data they contain*. Thanks to the freedoms afforded to us by free and
open source software, this noncustodial personal computing strategy is much
more approachable than one might be led to believe. With self-hosting, your
data can not be analyzed, monetized, used to weaponize an AI against you or
sold. Self-hosting returns agency to the individual by giving them true
ownership of their most sensitive private property. Self-hosting gives you
ownership and the maximum amount of control over all things important and
digital.


# Pricing

One of the core ideas we have for building the Mosaic platform is that it should be *sustainable software*.
What does that mean? It means that we want to build an environment where it is possible to sustainably
develop custom software for. Our focus on open-source does not preclude that developers need to be rewarded
for their effort. Instead, we want to build a marketplace that makes it easy and obvious to purchase software
written by developers at a fair market price.

## Pricing Models

There are two pricing models typically in use with modern software:

- **One-time purchase**, where the user purchases a specific software and then gets a perpetual license
  to use the software.
- **Subscription model**, where the user rents out the software by making monthly payments and loses
  access to the software once payments stop.

We would like to push a kind of in-between model that we believe is the best option for both developers
and for users. This is part of what makes the software *sustainable* and borrows some ideas from current
developments, such as crowdfunding campaigns.

We believe that users should be able to buy software and get **ownership**. This means that when a user
purchases a specific software, for example version 14.7.2 of Jack's *Taxes Plus*, the user should be able
to use that version forever. 

But how does Jack fund future development and the development of new features for his software? We believe
that a micropayment model makes sense here, where Jack can use a modern, rapid release cycle to release
new versions of his software weekly, and users can purchase these small updates at cent-prices. This 
generates a recurring income stream for Jack, who is incentivised to keep on improving his application,
and the user can essentially cast a vote on funding further development by the choice to keep subscribed.

In order to incentivise the subscription, subscribers can get software cheaper than waiting and purchasing
it when a new major version comes out. Jack can even decide to give some of his users the option to purchase
a subscription to alpha releases, which he produces nightly, at a discounted rate in exchange for feedback
and bug reports. This creates a win-win situation for the users, which get to own their software and have
free choice in how they want to purchase and support it, and Jack, who gets an income stream and instant
user feedback on where the development of his application is heading.

We believe that if the market is set out to be fair, the community is able to produce the highest quality
of software.

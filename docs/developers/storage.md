# Storage

One of the core pieces of this platform is the storage solution. This is currently implemented
as a Docker storage plugin, which stores all of your data in BTRFS volumes. It periodically
takes snapshots of the volumes it manages and uploads them. This way, any volume can be recreated
at any time from the previously uploaded snapshots.

This means that the storage plugin combines:

- A backup solution, by backing up snapshots continuously
- A failover solution, because volumes can instantly be recreated on a different machine
- A time machine, because the state of the volume (application) can be recreated at a
  previous time.

## Overview

Every volume has a private key, which is simply an ed25519 private key, typically this is base64-encoded
for displaying it. And example is `sETuhFwbQf9NJVAEOhu/QFhhhmKxY+f4VNFrO2IowGk=`.

The data for the volume is stored at `/var/lib/fractal/volumes/<pubkey>/data`. Snapshots are stored
at `/var/lib/fractal/volumes/<pubkey>/snapshots/<uuid>`. 

Periodically, the plugin will take snapshots of the data BTRFS subvolume, and encrypt and upload them
to IPFS. When it has done so, it posts a manifest to the storage backend with some metadata of the snapshot.

## Encryption

![Storage encryption](../media/storage-encryption.png)

## Installing the Storage Plugin

To install the storage plugin, simply use the Docker CLI client. Note that the Hive device will install
the plugin for you, so installing it manually is not normally necessary.

```
# on x86
$ docker plugin install fractalnetworks/storage:main
# on arm64 (raspberry pi)
$ docker plugin install fractalnetworks/storage:main-arm64
```

## Updating the Storage Plugin

In order to update the storage plugin, all of the containers that mount any volumes that it manages need
to be stopped. Once that is done, it can be deactivated and updated.

```
$ docker plugin disable fractalnetworks/storage:main
$ docker plugin update fractalnetworks/storage:main
```

Again, this is typically something that the Hive device handles for you and does not typically need to be
done manually.

## Creating a Volume



# Connectivity

Connectivity (and the selfhosted gateway) are the incoming connectivity solution for Mosaic. What does this mean?
When you use an application hosted in the cloud, it is always available. When you host an application at home, by default
it is only available from within the security of your own home, due to the way internet routers work.

Now, obviously you do want your application to be available from *outside* your home, so that you can use it collaboratively
with friends, for example. Or just so that you can access it when you are not home. This is called *incoming connectivity*,
because you can always connect to things outside from your home, but things from outside by default cannot connect to anything
*inside* your home.

## How it works

Connectivity or the selfhosted gateways work by running in the cloud, with a public IP that is always reachable. When it
gets an incoming connection, it looks at the domain that the connection is trying to reach. Even though the connection is
encrypted, it can still access the domain name that it is for by looking at the SNI header.

Your applications run a little Docker container called the *link container*, which maintains a WireGuard connection to the
backend. When the gateway gets an incoming connection, it routes that request into the right WireGuard connection, and it
pops out at the link container, where the request can be decrypted.


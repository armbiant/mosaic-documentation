title: Fractal Networks Mosaic
---

# <img src="media/fractal-wordmark.svg" alt="Fractal Networks" style="height: 1.7ex;" /> Mosaic

Welcome to Fractal Mosaic's documentation. Mosaic is a **noncustodial**,
**free** and **open source personal computing platform** designed to make
self-hosting accessible and survivable. 

<div class="grid cards" markdown>

-   :material-security:{ .lg .middle } **Security built-in**

    ---

    Fractal Mosaic uses end-to-end encryption to ensure data security and secrecy.

    [:octicons-arrow-right-24: Storage][storage]

-   :material-scale-balance:{ .lg .middle } **Open Source, GPL**

    ---

    Fractal Mosaic is licensed as GPL and is available on [GitLab][repo].

    [:octicons-arrow-right-24: License][repo]

-   :material-security-network:{ .lg .middle } **Connectivity**

    ---

    Fractal Mosaic makes sure your applications are always reachable, no matter where you go.

    [:octicons-arrow-right-24: Philosophy][repo]

-   :material-cloud-off:{ .lg .middle } **Non-Custodial**

    ---

    Fractal Mosaic is noncustodial, meaning that nobody but *you* owns your data.

    [:octicons-arrow-right-24: Philosophy][repo]

[repo]: https://gitlab.com/fractalnetworks/fractal-mosaic
[storage]: developers/storage.md

</div>


It works by running applications using industry-standard **Docker containers**,
with a WireGuard based **end-to-end encrypted** connectivity solution that
gives you a unique domain (such as `hasty-hopper.fractal.pub`) to make sure
your applications are always reachable, and an **end-to-end encrypted** storage
solution that continuously creates snapshots and backups of your data, allowing
your applications to be restored automatically at any time and without hassle.
This makes Fractal Mosaic the only **ephemeral self-hosting solution**: you
can, at any time, wipe your hard drive and re-install Fractal Mosaic and all of
your applications and data will be restored back, exactly as they were.

If you aren't familiar with the concept of self-hosting, we recommend you start
here: [What is Self-hosting?](intro/selfhosting.md).

## Installation

!!! warning "Fractal Mosaic is still under development"

    Please note that Fractal Mosaic is still under development, which means that
    not all features might be working correctly. We will do our best to point out
    features that are experimental. If you do encounter some issues, we would
    really appreciate it if you reached out to us and notified us of it so we can
    fix it or add a test for it.

If you haven't already, you can install it on a Linux machine using Docker
(recommended route), on a Windows machine using our WSL2 installer (currently
experimental) or support the development by backing our Kickstarter and
purchasing a Fractal Box, which comes with the software pre-installed.

<div class="grid cards" markdown>

- :fontawesome-brands-docker: **Docker** installation

- :fontawesome-brands-windows: **Windows** installation

- :material-server: **Box** Kickstarter

</div>

## Quick Start

Dive into the  [Mosaic Overview](mosaic/overview.md) to learn more about how
Mosaic works under the hood.

Mosaic is currently in its alpha preview release. During alpha, Mosaic's
backend will be hosted by [Fractal Networks](https://fractalnetworks.co).
Because Mosaic employs a decentralized architecture, self-hosted services do
not depend on the Mosaic backend remaining available if you deploy the
[selfhosted-gateway](https://github.com/fractalnetworksco/selfhosted-gateway).

Mosaic's Backend will be open sourced with the release of the public beta
sometime in 2023 depending on the rate of Mosaic's adoption and the strength of
its community.

You can get started with Mosaic at
[console.fractalnetworks.co](https://console.fractalnetworks.co).

## For Developers

Get started with [deploying your own applications to Mosaic](developers).

Mosaic provides a serverless deployment experience across a reliable on-prem
mesh of devices. Publish any existing Docker Compose based applications to the
Mosaic App Store to grow your user base (coming soon).

